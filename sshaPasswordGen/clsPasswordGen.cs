﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace sshaPasswordGen
{
	public class clsPasswordGen
	{
		private static int _saltSize = 4;

		public clsPasswordGen()
		{
		}

		public static string generateHash(string hashAlgorithm, string value)
		{
			HashAlgorithm algorithm = getHashAlgorithm(hashAlgorithm, null);

			byte[] valueBytes = Encoding.ASCII.GetBytes(value);
			byte[] hashValue = algorithm.ComputeHash(valueBytes);

			return Convert.ToBase64String(hashValue);
		}

		/// <summary>
		/// Generate a salted password hash
		/// Sample code found here: https://blogs.msdn.microsoft.com/alextch/2012/05/12/sample-c-code-to-create-sha1-salted-ssha-password-hashes-for-openldap/
		///		also here: http://www.obviex.com/samples/hash.aspx
		/// </summary>
		/// <param name="hashAlgorithm">must be one of the following values: SHA1, SHA256, SHA384, SHA512</param>
		/// <param name="password">plain text password</param>
		/// <param name="saltBytes">[optional] if not passed in random salt will be generated</param>
		/// <returns>hashed password</returns>
		public static string generateSaltedPassword(string hashAlgorithm, string password, byte[] saltBytes = null)
		{
			if (saltBytes == null)
			{
				string salt = string.Empty;
				saltBytes = generateSalt(_saltSize, out salt);
			}

			HashAlgorithm algorithm = getHashAlgorithm(hashAlgorithm, saltBytes);
			byte[] pwdBytes = Encoding.ASCII.GetBytes(password);

			byte[] pwdSaltedBytes = appendByteArray(pwdBytes, saltBytes);
			byte[] saltedPassword = algorithm.ComputeHash(pwdSaltedBytes);
			byte[] saltedPasswordWithSalt = appendByteArray(saltedPassword, saltBytes);

			return Convert.ToBase64String(saltedPasswordWithSalt);
		}

		/// <summary>
		/// Validate a plain text password against a hashed password
		/// </summary>
		/// <param name="hashAlgorithm">must be one of the following values: SHA1, SHA256, SHA384, SHA512</param>
		/// <param name="password"></param>
		/// <param name="saltedPassword"></param>
		/// <returns></returns>
		public static bool verifySaltedPassword(string hashAlgorithm, string password, string saltedPassword)
		{
			bool rval = false;
			string hashedPassword = saltedPassword;

			if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(saltedPassword))
			{
				return rval;
			}

			byte[] saltedPasswordWithSalt = Convert.FromBase64String(hashedPassword);
			byte[] saltBytes = new byte[_saltSize];
			int hashSizeInBytes = getHashSizeInBytes(hashAlgorithm);

			if (saltedPasswordWithSalt.Length < hashSizeInBytes + _saltSize)
				return rval;

			for (int i = 0; i < _saltSize; i++)
			{
				saltBytes[i] = saltedPasswordWithSalt[hashSizeInBytes + i];
			}

			//strip off the {SHA1} definition from the front of the string
			string expectedHashString = generateSaltedPassword(hashAlgorithm, password, saltBytes);

			return (saltedPassword == expectedHashString);
		}

		/// <summary>
		/// Canvas uses SHA1, so we will force this algorithm and not allow selection
		/// </summary>
		/// <param name="password"></param>
		/// <param name="salt">if salt is not passed it will be generated</param>
		/// <returns></returns>
		public static string generateCanvasSaltedPassword(string password, string salt = null)
		{
			string rval = string.Empty;

			byte[] pwdBytes = Encoding.ASCII.GetBytes(password);
			byte[] saltBytes;

			if (string.IsNullOrEmpty(salt))
				saltBytes = generateSalt(_saltSize, out salt);
			else
				saltBytes = Encoding.ASCII.GetBytes(salt);

			SHA1Managed algorithm = new SHA1Managed();
			byte[] pwdSalted = appendByteArray(pwdBytes, saltBytes);
			byte[] hashPwd = algorithm.ComputeHash(pwdSalted);
			string hexString = ToHexString(hashPwd) + salt;

			byte[] encoded = System.Text.Encoding.UTF8.GetBytes(hexString);
			rval = System.Convert.ToBase64String(encoded);

			return rval;
		}

		/// <summary>
		/// Use this method to verify the computed hashed password
		/// </summary>
		/// <param name="password">plain text password to verify</param>
		/// <param name="saltedPassword">hashed password to compare to</param>
		/// <returns></returns>
		public static bool verifyCanvasSaltedPassword(string password, string saltedPassword)
		{
			//reverse the base64 string
			byte[] decoded = Convert.FromBase64String(saltedPassword);
			StringBuilder hexString = new StringBuilder();
			foreach (byte b in decoded)
			{
				hexString.Append((char)b);
			}
			string salt = hexString.ToString().Substring(hexString.Length - 4, 4);
			string generatedHash = generateCanvasSaltedPassword(password, salt);

			return (generatedHash == saltedPassword);
		}

		/// <summary>
		/// Used during testing to match steps taken by sample code from Canvas python script
		/// https://github.com/kajigga/canvas-contrib/tree/master/SIS_Integration/create_ssha_passwords
		/// This method uses a default password: "password" and salt: "asdf" to test for consistent output
		/// </summary>
		/// <param name="hashAlgorithm"></param>
		/// <param name="password"></param>
		/// <param name="saltBytes"></param>
		/// <returns></returns>
		public static string generateHMAC_SaltedPassword()
		{
			string rval = string.Empty;
			string salt = "asdf";

			byte[] pwdBytes = Encoding.ASCII.GetBytes("password");
			byte[] saltBytes = Encoding.ASCII.GetBytes("asdf");

			SHA1Managed algorithm = new SHA1Managed();
			byte[] pwdSalted = appendByteArray(pwdBytes, saltBytes);
			byte[] hashPwd = algorithm.ComputeHash(pwdSalted);
			string hexString = ToHexString(hashPwd) + salt;

			byte[] encoded = System.Text.Encoding.UTF8.GetBytes(hexString);
			rval = System.Convert.ToBase64String(encoded);


			//reverse the base64 string
			byte[] decoded = Convert.FromBase64String(rval);
			StringBuilder s = new StringBuilder();
			foreach (byte b in decoded)
			{
				s.Append((char)b);
			}

			bool isMatch = (s.ToString() == hexString);

			return rval;
		}

		#region PRIVATE METHODS
		private static string ToHexString(byte[] hash)
		{
			StringBuilder hex = new StringBuilder(hash.Length * 2);
			foreach (byte b in hash)
			{
				hex.AppendFormat("{0:x2}", b);
			}
			return hex.ToString();
		}

		/// <summary>
		/// Return a valid HashAlgorithm based on the value passed in
		/// </summary>
		/// <param name="hashAlgorithm"></param>
		/// <returns>returns NULL if the passed argument does not match a valid type</returns>
		private static HashAlgorithm getHashAlgorithm(string hashAlgorithm, byte[] key)
		{
			HashAlgorithm ha = null;

			switch (hashAlgorithm.ToUpper())
			{
				case "SHA1":
					{
						ha = new SHA1Managed();
					}
					break;

				case "SHA256":
					{
						ha = new SHA256Managed();
					}
					break;

				case "SHA384":
					{
						ha = new SHA384Managed();
					}
					break;

				case "SHA512":
					{
						ha = new SHA512Managed();
					}
					break;

				case "HMAC-SHA1":
					{
						ha = new HMACSHA1();
					}
					break;

				case "HMAC-SHA256":
					{
						ha = new HMACSHA256();
					}
					break;

				case "HMAC-SHA384":
					{
						ha = new HMACSHA384();
					}
					break;

				case "HMAC-SHA512":
					{
						ha = new HMACSHA512();
					}
					break;
			}
			return ha;
		}

		private static int getHashSizeInBytes(string hashAlgorithm)
		{
			int hashBytes = 0;

			switch (hashAlgorithm.ToUpper())
			{
				case "SHA1":
				case "HMAC-SHA1":
					{
						hashBytes = 20; // 160 bits / 8 bits per byte
					}
					break;

				case "SHA256":
				case "HMAC-SHA256":
					{
						hashBytes = 32;
					}
					break;

				case "SHA384":
				case "HMAC-SHA384":
					{
						hashBytes = 48;
					}
					break;

				case "SHA512":
				case "HMAC-SHA512":
					{
						hashBytes = 64;
					}
					break;
			}
			return hashBytes;
		}

		private static byte[] generateSalt(int saltSize, out string salt)
		{
			//RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			Random random = new Random();

			const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
			salt = new string(Enumerable.Repeat(chars, _saltSize)
			  .Select(s => s[random.Next(s.Length)]).ToArray());

			byte[] buff = Encoding.ASCII.GetBytes(salt);
			return buff;
		}

		private static byte[] appendByteArray(byte[] pwd, byte[] salt)
		{
			byte[] byteResult = new byte[pwd.Length + salt.Length];

			for (int i = 0; i < pwd.Length; i++)
			{
				byteResult[i] = pwd[i];
			}

			for (int i = 0; i < salt.Length; i++)
			{
				byteResult[pwd.Length + i] = salt[i];
			}

			return byteResult;
		}
		#endregion

	}
}
