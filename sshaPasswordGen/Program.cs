﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sshaPasswordGen
{
	class Program
	{
		static void Main(string[] args)
		{
			clsPasswordGen pgen = new clsPasswordGen();
			string algorithm = "SHA1";

			Console.WriteLine("What algorithm do you want to use:");
			Console.WriteLine("    1 - SHA1");
			Console.WriteLine("    2 - SHA256");
			Console.WriteLine("    3 - SHA384");
			Console.WriteLine("    4 - SHA512");
			Console.Write("Type the number and press <Enter>: ");
			ConsoleKeyInfo key = Console.ReadKey();
			switch(key.Key)
			{
				case ConsoleKey.D1:
					{
						algorithm = "SHA1";
					}
					break;

				case ConsoleKey.D2:
					{
						algorithm = "SHA256";
					}
					break;

				case ConsoleKey.D3:
					{
						algorithm = "SHA384";
					}
					break;

				case ConsoleKey.D4:
					{
						algorithm = "SHA512";
					}
					break;

				default:
					{
						Console.WriteLine("Unrecognized option...defaulting to[" + algorithm + "]");
					}
					break;
			}
			Console.WriteLine();
			Console.Write("Type in the password to encrypt: ");

			string password = Console.ReadLine();
			Console.WriteLine("You entered: " + password);
			Console.WriteLine();

			string hashedPwd = clsPasswordGen.generateSaltedPassword(algorithm, password);
			string canvasPwd = clsPasswordGen.generateCanvasSaltedPassword(password);

			Console.WriteLine("Standard Hashed Password:  " + hashedPwd);
			Console.Write("Verify Password Result:  ");
			if (clsPasswordGen.verifySaltedPassword(algorithm, password, hashedPwd))
				Console.WriteLine("VERIFIED");
			else
				Console.WriteLine("FAILED");
			Console.WriteLine();

			Console.WriteLine("Canvas Hashed Password:  " + canvasPwd);
			Console.Write("Veriviy Canvas Password:  ");
			if (clsPasswordGen.verifyCanvasSaltedPassword(password, canvasPwd))
				Console.WriteLine("VERIFIED");
			else
				Console.WriteLine("FAILED");
			Console.ReadKey();
		}
	}
}
