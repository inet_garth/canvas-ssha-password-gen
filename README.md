# README #

This project was published as a companion to the following article: [.NET - Generating Encrypted User Passwords](https://community.canvaslms.com/groups/canvas-developers/blog/2016/11/13/net-generating-encrypted-user-passwords)

All code is provided as-is for demo purposes only. This project was created as a demo, and is not considered production ready. You will need to modify this code to meet your specific requirements.

### What is this repository for? ###

The purpose of this project is to demonstrate how to generate encrypted passwords.  There are specific methods for meeting requirements for the Canvas platform.  Several algorithms are implemented for the purpose of illustration.

### How do I get set up? ###

If you have a copy of Visual Studio, you should be able to download the solution, compile, and run. Run the compiled exe from the command line, you will be prompted for input.